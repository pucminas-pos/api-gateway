FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/api-gateway/src
COPY pom.xml /home/api-gateway
RUN mvn -f /home/api-gateway/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/api-gateway/target/api-gateway.jar /usr/local/lib/api-gateway.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/api-gateway.jar"]
