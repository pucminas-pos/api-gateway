package br.com.boasaude.apigateway.infrastructure.util;

import br.com.boasaude.apigateway.infrastructure.exception.UnauthorizedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.function.Supplier;

public class ResponseUtil {

    private ResponseUtil(){}

    public static String retrieveHeader(HttpHeaders headers, String paramHeader, String errorMessage, HttpStatus status) {
        return Optional.ofNullable(headers.get(paramHeader)).orElseThrow(exceptionSupplier(errorMessage, status))
                .stream().findFirst().orElseThrow(exceptionSupplier(errorMessage, status));
    }

    public static Supplier<RuntimeException> exceptionSupplier(String message, HttpStatus status) {
        return () -> {
            if (status == HttpStatus.UNAUTHORIZED)
                return new UnauthorizedException(message, HttpStatus.UNAUTHORIZED);

            return new RuntimeException(message);
        };
    }
}
