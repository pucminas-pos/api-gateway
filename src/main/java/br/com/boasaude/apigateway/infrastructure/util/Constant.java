package br.com.boasaude.apigateway.infrastructure.util;

public class Constant {

    private Constant() {}

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String X_USER_ID_HEADER = "X-User-Id";
    public static final String X_USER_ROLE_HEADER = "X-User-Role";

    public static final String HEADER_NOT_PROVIDED_MESSAGE = "authorization header not provided";
    public static final String HEADER_MALFORMATTED = "authorization header malformatted";
    public static final String USER_NOT_AUTHORIZED_MESSAGE = "user not authorized to access this endpoint";



}
