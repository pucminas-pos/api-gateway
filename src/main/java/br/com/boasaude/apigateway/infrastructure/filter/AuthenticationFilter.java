package br.com.boasaude.apigateway.infrastructure.filter;

import br.com.boasaude.apigateway.domain.model.external.authapi.User;
import br.com.boasaude.apigateway.domain.service.AuthApiService;
import br.com.boasaude.apigateway.infrastructure.exception.UnauthorizedException;
import br.com.boasaude.apigateway.infrastructure.util.ResponseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Collections;

import static br.com.boasaude.apigateway.infrastructure.util.Constant.*;

@Slf4j
@Component
public class AuthenticationFilter extends AbstractGatewayFilterFactory {

    private static final String REGEX_TOKEN = "^Bearer\\s.+$";

    @Autowired
    private AuthApiService authApiService;

    @Override
    public GatewayFilter apply(Object config) {
        return ((exchange, chain) -> {
            HttpHeaders headers = exchange.getRequest().getHeaders();
            String authToken = ResponseUtil.retrieveHeader(headers, AUTHORIZATION_HEADER, HEADER_NOT_PROVIDED_MESSAGE, HttpStatus.UNAUTHORIZED);

            if (!validateFormat(authToken)){
                throw new UnauthorizedException(HEADER_MALFORMATTED, HttpStatus.UNAUTHORIZED);
            }

            User user = authApiService.validateToken(getToken(authToken));

            ServerHttpRequest request = exchange.getRequest().mutate().headers(h -> {
                h.putAll(headers);
                h.put(X_USER_ID_HEADER, Collections.singletonList(user.getEmail()));
                h.put(X_USER_ROLE_HEADER, Collections.singletonList(user.getRole().toString()));
            }).build();

            return chain.filter(exchange.mutate().request(request).build());
        });
    }

    private boolean validateFormat(String authToken) {
        return authToken.matches(REGEX_TOKEN);
    }

    private String getToken(String authToken) {
        return authToken.split(" ")[1];
    }
}
