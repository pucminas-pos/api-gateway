package br.com.boasaude.apigateway.infrastructure.filter;

import br.com.boasaude.apigateway.domain.model.external.authapi.Role;
import br.com.boasaude.apigateway.infrastructure.exception.UnauthorizedException;
import br.com.boasaude.apigateway.infrastructure.util.ResponseUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static br.com.boasaude.apigateway.infrastructure.util.Constant.*;

@Slf4j
@Component
public class AuthorizationFilter extends AbstractGatewayFilterFactory<AuthorizationFilter.AuthorizationConfig> {

    private static final String PARAMETER_FILTER = "role";

    public AuthorizationFilter() {
        super(AuthorizationFilter.AuthorizationConfig.class);
    }

    @Override
    public GatewayFilter apply(AuthorizationConfig config) {
        return ((exchange, chain) -> {
            HttpHeaders headers = exchange.getRequest().getHeaders();

            Role role = Role.valueOf(ResponseUtil.retrieveHeader(headers, X_USER_ROLE_HEADER, USER_NOT_AUTHORIZED_MESSAGE, HttpStatus.UNAUTHORIZED));

            if ((role != Role.ADMIN) && (Objects.isNull(config) || role != config.getRole())) {
                throw new UnauthorizedException(USER_NOT_AUTHORIZED_MESSAGE, HttpStatus.UNAUTHORIZED);
            }

            return chain.filter(exchange);
        });
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList(PARAMETER_FILTER);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class AuthorizationConfig {
        private Role role;
    }
}
