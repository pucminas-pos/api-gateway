package br.com.boasaude.apigateway.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends IntegrationException {

    public UnauthorizedException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
