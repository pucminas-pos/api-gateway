package br.com.boasaude.apigateway.infrastructure.handler;

import br.com.boasaude.apigateway.infrastructure.exception.IntegrationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebExceptionHandler;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Order(-3)
@Component
public class ExceptionHandler implements WebExceptionHandler {

    private static final String MESSAGE_INTERNAL_SERVER_ERROR = "unexpected error. try again later";

    @Autowired
    private ObjectMapper objectMapper;


    @SneakyThrows
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        exchange.getResponse().getHeaders().put(HttpHeaders.CONTENT_TYPE, List.of(MediaType.APPLICATION_JSON.toString()));

        if (ex instanceof IntegrationException) {
            IntegrationException integrationEx = (IntegrationException) ex;
            return buildResponse(exchange, integrationEx.getHttpStatus(), integrationEx.getMessage());
        }

        log.error("error", ex);
        return buildResponse(exchange, HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE_INTERNAL_SERVER_ERROR);
    }

    private Mono<Void> buildResponse(ServerWebExchange exchange, HttpStatus status, String errorMessage) throws JsonProcessingException {
        exchange.getResponse().setStatusCode(status);
        byte[] body = objectMapper.writeValueAsBytes(createErrorMap(errorMessage));
        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(body);
        return exchange.getResponse().writeWith(Flux.just(buffer));
    }

    private static Map createErrorMap(String message) {
        Map<String, String> map = new HashMap<>();
        map.put("message", message);
        map.put("date", LocalDateTime.now().toString());
        return map;
    }
}
