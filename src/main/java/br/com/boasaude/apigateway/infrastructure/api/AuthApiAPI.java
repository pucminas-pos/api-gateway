package br.com.boasaude.apigateway.infrastructure.api;

import br.com.boasaude.apigateway.domain.model.external.authapi.User;
import br.com.boasaude.apigateway.infrastructure.exception.AuthServiceIntegrationException;
import br.com.boasaude.apigateway.infrastructure.exception.UnauthorizedException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "authapi-api", url = "${application.auth-api.url}", configuration = {AuthApiAPI.AuthApiErrorDecoder.class})
public interface AuthApiAPI {

    @PostMapping("authentication/token")
    User validateToken(@RequestHeader("Authorization") String authToken);


    class AuthApiErrorDecoder implements ErrorDecoder {

        @Override
        public Exception decode(String method, Response response) {
            final HttpStatus status = HttpStatus.valueOf(response.status());

            if (status == HttpStatus.UNAUTHORIZED) {
                return new UnauthorizedException("user unauthorized. Token not provided or expired", HttpStatus.UNAUTHORIZED);
            }
            return new AuthServiceIntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
