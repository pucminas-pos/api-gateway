package br.com.boasaude.apigateway.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class AuthServiceIntegrationException extends IntegrationException {

    public AuthServiceIntegrationException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
