package br.com.boasaude.apigateway.domain.service;

import br.com.boasaude.apigateway.domain.model.external.authapi.User;

public interface AuthApiService {

    User validateToken(String authToken);
}
