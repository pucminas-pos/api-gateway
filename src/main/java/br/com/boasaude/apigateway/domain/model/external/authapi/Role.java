package br.com.boasaude.apigateway.domain.model.external.authapi;

public enum Role {
    ADMIN, ASSOCIATE

}
