package br.com.boasaude.apigateway.domain.service.impl;

import br.com.boasaude.apigateway.domain.model.external.authapi.User;
import br.com.boasaude.apigateway.domain.service.AuthApiService;
import br.com.boasaude.apigateway.infrastructure.api.AuthApiAPI;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@NoArgsConstructor
@AllArgsConstructor
public class AuthApiServiceImpl implements AuthApiService {

    @Autowired
    private AuthApiAPI authApiAPI;


    @Override
    @Cacheable(value="br.com.boasaude.apigateway.authapi.validateToken", key = "#authToken", unless = "#result == null")
    public User validateToken(String authToken) {
        return authApiAPI.validateToken(authToken);
    }
}
